var fs = require('fs');
var request = require('sync-request');
var yaml = require('js-yaml');
var hash = require('object-hash');
var underscore = require('underscore');

var variables = yaml.safeLoad(fs.readFileSync('./statusVariables.yaml', 'utf8'));

// console.log("Doc =>", variables);
var res = undefined;
var responseEmtpy = false;
var insertResult;

variables.models.forEach(model => {
  responseEmtpy = false;
  res = request('GET', 'http://18.204.100.238:5000/api/execute', {
    qs: {
      query: `SELECT MAX(${model.indexName}) FROM ${model.modelName};`,
    }
  });
  let maxIndexValue = JSON.parse(res.getBody('utf-8')).recordset[0][''];
  while (model.lastIndex < maxIndexValue) {

    res = request('GET', 'http://18.204.100.238:5000/api/execute', {
      qs: {
        query: `SELECT * FROM ${model.modelName} WHERE ${model.indexName} = ${model.lastIndex}
                ORDER BY ${model.indexName} OFFSET ${model.page} ROWS FETCH NEXT ${variables.limit} ROWS ONLY;`,
      }
    });
    result = JSON.parse(res.getBody('utf-8')).recordset;

    console.log("Current Record =>", model.lastIndex);
    console.log("Length =>", result.length);
    console.log("============================================");

    model.lastIndex += 1;
    
    // TODO: Uncomment
    fs.writeFileSync('./statusVariables.yaml', yaml.safeDump(variables));

  console.log("Res =>", result);

    res = request('POST', `http://localhost:8000/api/${model.insertPath}`, {
      json: result,
    });
    try {
      insertResult = JSON.parse(res.getBody('utf-8'));
    } catch (error) {
      console.log("ERR =>", error);
    }
  }
});